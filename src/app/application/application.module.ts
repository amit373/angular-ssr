import { CommonModule } from '@angular/common';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { MaterialModule } from '../shared/material/material.module';
import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationComponent } from './application.component';

@NgModule({
  declarations: [ApplicationComponent],
  imports: [
    CommonModule,
    ApplicationRoutingModule,
    MaterialModule,
    HttpClientModule, // (Required) For share counts
    HttpClientJsonpModule, // (Optional) Add if you want tumblr share counts
    ShareButtonsModule,
    NgbModule,
    NgbAlertModule
  ]
})
export class ApplicationModule {}
