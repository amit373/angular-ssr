import { Component, OnInit } from '@angular/core';
import { MetaTagService } from './../service/meta-tag.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  constructor(private metaTagService: MetaTagService) { }

  ngOnInit() {
    const metaData: any = {
      title: 'Application Page',
      description: 'This is a application page description!.',
      image: 'https://images.pexels.com/photos/3614358/pexels-photo-3614358.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
      card: 'Card Detail For Twitter',
      article: 'article',
      url: 'www.google.com/admin',
      site_name: 'Ni Pata Site'
    };
    this.metaTagService.setMetaTage(metaData);
    this.metaTagService.setTitle('Application Page');
  }
}
