import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MetaTagService } from '../service/meta-tag.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private metaTagService: MetaTagService
  ) {
    const metaData: any = {
      title: 'Home Page',
      description: 'This is a Home page description!.',
      image: 'https://images.pexels.com/photos/3617467/pexels-photo-3617467.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      card: 'Card Detail For Twitter',
      article: 'article',
      url: 'www.google.com/admin',
      site_name: 'Ni Pata Site'
    };
    this.metaTagService.setMetaTage(metaData);
    this.metaTagService.setTitle('Admin Page');
  }

  showSuccess() {
    this.toastr.success('Hello Home!', 'Toastr fun!');
  }

  ngOnInit() {
    this.showSuccess();
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 3000);
  }
}
