import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MetaTagService } from '../service/meta-tag.service';
import { MustMatch } from '../shared/must-match.validator';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private metaTagService: MetaTagService
  ) {
    const metaData: any = {
      title: 'Admin Page',
      description: 'This is a Admin page description!.',
      image: 'https://images.pexels.com/photos/3560024/pexels-photo-3560024.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      card: 'Card Detail For Twitter',
      article: 'article',
      url: 'www.google.com/admin',
      site_name: 'Ni Pata Site'
    };
    this.metaTagService.setMetaTage(metaData);
    this.metaTagService.setTitle('Admin Page');
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        title: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required],
        acceptTerms: [false, Validators.requiredTrue]
      },
      {
        validator: MustMatch('password', 'confirmPassword')
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    // display form values on success
    alert(
      'SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4)
    );
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
