import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable()
export class MetaTagService {
  constructor(private meta: Meta, private title: Title) {}

  setMetaTage(data: any) {
    // this.meta.addTag({ name: 'title', content: data.title });
    // this.meta.addTag({ name: 'description', content: data.description });

    this.meta.updateTag({ property: 'og:type', content: data.article });
    this.meta.updateTag({ property: 'og:site_name', content: data.site_name});
    this.meta.updateTag({ property: 'og:title', content: data.title });
    this.meta.updateTag({ property: 'og:description', content: data.description });
    this.meta.updateTag({ property: 'og:image', content: data.image });
    this.meta.updateTag({ property: 'og:url', content: data.url });

    this.meta.updateTag({ name: 'twitter:card', content: data.card });
    this.meta.updateTag({ name: 'twitter:site', content:  data.site_name  });
    this.meta.updateTag({ name: 'twitter:title', content: data.title });
    this.meta.updateTag({ name: 'twitter:description', content: data.description });
    this.meta.updateTag({ name: 'twitter:image', content: data.image });
  }

  setTitle(title: string) {
    this.title.setTitle(title);
  }
}
